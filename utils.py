import os, os.path
import signal
import sqlite3

from tornado.ioloop import IOLoop, PeriodicCallback


def path(*args):
    print('used:', os.path.abspath(os.path.join(*args)))
    return os.path.abspath(os.path.join(*args))


class SigExit:
    def __init__(self):
        self.is_closing = False

    def enable(self):
        def signal_handler(signum, frame):
            self.is_closing = True

        def try_exit():
            if self.is_closing:
                IOLoop.instance().stop()
                get_db().close()
        signal.signal(signal.SIGINT, signal_handler)
        PeriodicCallback(try_exit, 100).start()


class DBHolder:
    def __init__(self):
        try:
            dbinit = not os.path.exists(path('db/note.db'))
            db = sqlite3.connect(path('db/note.db'))
            cur = db.cursor()
            if dbinit:
                cur.execute('create table note(date text, content text)')
                db.commit()
            self.db = db
            self.cursor = cur
        except sqlite3.OperationalError as e:
            print('Fatal Error on DBHolder:', e)

    def insert(self, content):
        try:
            cur = self.cursor
            date = cur.execute("select datetime('now')").fetchall()[0][0]
            cur.execute('insert into note(date,content) values (?,?)', (date, content))
            if cur.rowcount <= 0:
                raise ValueError('rowcount is', cur.rowcount)
            print('insert', date, ':', content)
            return True
        except (sqlite3.OperationalError, ValueError) as e:
            print('insert failed:', e)
            return False

    def select_all(self):
        try:
            res = self.cursor.execute(
                    "select rowid,date,content from note order by datetime(date) desc"
                ).fetchall()
            print('select all:')
            for (id, date, content) in res:
                print("\t{}\t{}".format(id, date))
            return res
        except sqlite3.OperationalError as e:
            print('select all failed:', e)
            return []

    def select(self, row_id):
        try:
            res = self.cursor.execute(
                "select date,content from note where rowid = ?", (row_id,)).fetchone()
            if not res:
                raise ValueError('cannot fetch the data')
            date, content = res
            print('select', row_id, ':', date)
            return date, content
        except (sqlite3.OperationalError, ValueError) as e:
            print('select', row_id, 'failed:', e)
            return None

    def update(self, row_id, content):
        try:
            res = self.cursor.execute(
                "update note set content = ? where rowid = ?", (content, row_id))
            if res.rowcount <= 0:
                raise ValueError('rowcount is', res.rowcount)
            print('update', row_id)
            return True
        except (sqlite3.OperationalError, ValueError) as e:
            print('update', row_id, 'failed:', e)
            return False

    def delete(self, row_id):
        try:
            res = self.cursor.execute(
                "delete from note where rowid = ?", (row_id,))
            if res.rowcount <= 0:
                raise ValueError('rowcount is', res.rowcount)
            print('delete', row_id)
            return True
        except (sqlite3.OperationalError, ValueError) as e:
            print('delete', row_id, 'failed:', e)
            return False

    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()

    def close(self):
        self.db.close()


db_instance = DBHolder()


def get_db():
    global db_instance
    if not db_instance:
        db_instance = DBHolder()
    return db_instance
