original_content = "";

function reset() {
	document.getElementById('content').innerHTML = original_content;
}

function erase() {
	document.getElementById('content').innerHTML = '';
}

function sendform(is_delete, content, id) {
	var form = document.createElement('form');
	form.setAttribute('method', 'post');
	form.setAttribute('action', '/edit/' + id);

	var isdelete = document.createElement('input');
	isdelete.setAttribute('type', 'hidden');
	isdelete.setAttribute('name', 'is-delete');
	isdelete.setAttribute('value', is_delete);

	var contentSender = document.createElement('input');
	contentSender.setAttribute('type', 'hidden');
	contentSender.setAttribute('name', 'content');
	contentSender.setAttribute('value', content);
	
	form.appendChild(isdelete);
	form.appendChild(contentSender);
	document.body.appendChild(form);
	form.submit();
}

function deleteNote(id) {
	if (confirm('글이 삭제됩니다. 계속하시겠습니까?')) {
		sendform('true', '', id);
	}
}

function submit(id) {
	var content = document.getElementById('content').innerText;
	if (content) {
		sendform('false', content, id);
	} else {
		alert('빈 글은 저장할 수 없습니다.');
	}
}

window.onload = function() {
	original_content = document.getElementById('content').innerHTML;
}