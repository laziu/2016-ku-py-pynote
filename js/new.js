function reset() {
	document.getElementById('content').innerHTML = '';
}

function submit() {
	var content = document.getElementById('content').innerText;
	if (content) {
		var form = document.createElement('form');
		form.setAttribute('method', 'post');
		form.setAttribute('action', '/new');

		var contentSender = document.createElement('input');
		contentSender.setAttribute('type', 'hidden');
		contentSender.setAttribute('name', 'content');
		contentSender.setAttribute('value', content);
		
		form.appendChild(contentSender);
		document.body.appendChild(form);
		form.submit();
	} else {
		alert('빈 글은 저장할 수 없습니다.');
	}
}

window.onload = function() {
	reset();
}