function ajax(type, url, success, error) {
	var xobj = new XMLHttpRequest();
	xobj.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			success(this.responseText);
		} else {
			error(this.responseText, this.status);
		}
	};
	xobj.open(type, url, true);
	xobj.send();
}