var view_mode_is_list = false;

function changeForm() {
	view_mode_is_list = !view_mode_is_list;
	if (view_mode_is_list) {
		document.getElementById('view-mode').innerHTML = 'view_list';
		document.getElementById('note-style').innerHTML = 
			'.note{width:calc(100% - 2rem);max-width:800px}';
	} else {
		document.getElementById('view-mode').innerHTML = 'view_module';
		document.getElementById('note-style').innerHTML = [
			'.note{',
            	'height: 300px;',
            	'width: 300px}',
			'.note .content{',
				'max-height: calc(300px - 5rem)}'
		].join("");
	}
}

window.onload = function() {
	if (document.getElementsByClassName('note').length == 0) {
		var maindiv = document.getElementById('main');
		maindiv.classList.add('blank');
		maindiv.innerHTML = "<div><p>('Δ')</p><p>노트가 없습니다.</p></div>";
	}
}