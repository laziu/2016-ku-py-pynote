from tornado.web import RequestHandler
from utils import path, get_db


class MainHandler(RequestHandler):
    def get(self):
        self.render(path('html/main.html'), note_list=get_db().select_all())


class EditHandler(RequestHandler):
    def get(self, id_str):
        try:
            id = int(id_str)
            res = get_db().select(id)
            if res:
                date, content = res
                self.render('html/edit.html', date=date, content=content, id=id)
            else:
                self.render('html/error.html', error='존재하지 않거나 삭제된 노트입니다.')
        except ValueError:
            self.render('html/error.html', error='잘못된 접근입니다.')

    def post(self, id_str):
        if self.get_argument('is-delete') == 'true':  # delete
            try:
                id = int(id_str)
                if get_db().delete(id):
                    get_db().commit()
                    self.redirect('/')
                else:
                    get_db().rollback()
                    self.render('html/error.html', error='이미 삭제된 노트입니다.')
            except ValueError:
                get_db().rollback()
                self.render('html/error.html', error='잘못된 접근입니다. 삭제에 실패했습니다.')
        else:  # update
            try:
                id = int(id_str)
                if get_db().update(id, self.get_argument('content')):
                    get_db().commit()
                    self.redirect('/')
                else:
                    get_db().rollback()
                    self.render('html/error.html', error='존재하지 않는 노트입니다. 수정에 실패했습니다.')
            except ValueError:
                get_db().rollback()
                self.render('html/error.html', error='잘못된 접근입니다. 수정에 실패했습니다.')


class NewHandler(RequestHandler):
    def get(self):
        self.render(path('html/new.html'))

    def post(self):
        content = self.get_argument('content')
        get_db().insert(content)
        get_db().commit()
        self.redirect('/')


class Error404Handler(RequestHandler):
    def get(self):
        self.render(path('html/error.html'), error='잘못된 접근입니다.')