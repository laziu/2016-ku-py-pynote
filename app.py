from tornado.ioloop import IOLoop
from tornado.web import Application, url, StaticFileHandler

from utils import SigExit
from pages import MainHandler, EditHandler, NewHandler, Error404Handler


def make_app():
    return Application([
        url(r"/font/(.*)", StaticFileHandler, {'path': './font/'}),
        url(r"/css/(.*)", StaticFileHandler, {'path': './css/'}),
        url(r"/js/(.*)", StaticFileHandler, {'path': './js/'}),
        url(r"/mdl/(.*)", StaticFileHandler, {'path': './libs/mdl/'}),
        url(r"/", MainHandler),
        url(r"/edit/([^/]+)", EditHandler),
        url(r"/new", NewHandler)
    ], default_handler_class=Error404Handler)


if __name__ == '__main__':
    fs = SigExit()
    fs.enable()
    app = make_app()
    app.listen(8080)
    IOLoop.instance().start()
